<?php



require_once "../../../vendor/autoload.php";



use App\Hobies\Hobies;
use App\Utility\Utility;

$obj = new Hobies();
$strHobbies = implode(", ", $_POST['hobie']);
$_POST['hobie'] = $strHobbies;


$obj->setData($_POST);

$obj->update();

Utility::redirect("index.php");