<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Hobies\Hobies;
use App\Utility\Utility;
use App\Message\Message;

if(!isset($_GET['id'])) {
    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}


$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

$obj = new Hobies();

$obj->setData($_GET);

$singleData = $obj->view();

$hobbiesArray = explode(", ", $singleData->hobies);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Add Form</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">
    <style>
        body {
            background: #c1e2b3;
        }
    </style>
</head>
<body>
<div class="container">

    <h1 style="color: #442a8d;">Hobies Information Edit </h1>

    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
        <strong> Please Enter Name:</strong>
        <input type="text" name="name" value="<?php echo $singleData->name ?>">
        <br>

        <strong>Please Enter Hobbies: </strong>

        <div class="hobie">
            <input type="checkbox" name="hobie[]" value="Cooking">Cooking
            <input type="checkbox" name="hobie[]" value="Coloring">Coloring
            <input type="checkbox" name="hobie[]" value="Drawing">Drawing
            <input type="checkbox" name="hobie[]" value="Fashion">Fashion
            <input type="checkbox" name="hobie[]" value="Magic">Magic
        </div>
        <div>
            <input type="submit" value="Update">
        </div>

    </form>


</div>
<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>