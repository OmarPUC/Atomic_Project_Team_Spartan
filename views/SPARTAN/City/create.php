<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/city.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>City Select</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <h1>City Add Form</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder="Enter Your Name..." required>
            </div>

            <div class="country">
               <select name="country" required>
                   <option value="" disabled selected>Please Select city...</option>
                   <option name="country" value="Dhaka">Dhaka</option>
                   <option name="country" value="Chittagong">Chittagong</option>
                   <option name="country" value="Barishal">Barishal</option>
                   <option name="country" value="Khulna">Khulna</option>
                   <option name="country" value="RajSahi">RajSahi</option>
                   <option name="country" value="RangPur">RangPur</option>
                   <option name="country" value="Syllet">Syllet</option>
                   <option name="country" value="Comilla">Comilla</option>
                   <option name="country" value="Narayenghonj">Narayenghonj</option>
                   <option name="country" value="Cox's Bazer">Cox's Bazer</option>

               </select>
           </div>
            <div>
                <input type="submit" value="Submit">
            </div>
            <div class="msg">
                <?php
                require_once ("../../../vendor/autoload.php");

                $msg = \App\Message\Message::message();

                echo "<div id='message'>".$msg."</div>";
                ?>
            </div>
        </form>
    </div>
</div>

</body>
</html>