<?php

use App\Message\Message;

use App\Utility\Utility;
require_once ("../../../vendor/autoload.php");


$obj = new \App\City\City();
$allData  =  $obj->trashed();
$msg = Message::message();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>city</title>

    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

</head>
<body class="container">

<?php echo "<div>  <div align='center' class=' alert-info ' id='message'>  $msg </div>   </div>";   ?>

<h1> Trashed List of - City </h1>
<a href="index.php" class="btn btn-primary">Back</a>


<table class="table table-bordered table-striped">
    <form  id="selectionForm" action="recover_multiple.php" method="post">

        <div class="nav navbar">

            <input class="btn btn-warning btn-lg" type="button" id="recoverMultipleButton" value="Recover Multiple">

            <tr>
                <th>Check All <input type='checkbox' id='select_all' name='select_all' value='$row->id'></th>
                <th>Serial</th>
                <th>ID</th>
                <th>Name</th>
                <th>City</th>
                <th>Action Buttons</th>
            </tr>

            <?php
            $serial = 1;

            foreach($allData as $row )
            {

                echo "

              <tr>
                   
                  <td><input type='checkbox' class='checkbox' name='multiple[]' value='$row->id'> </td>
                  <td>$serial</td>
                  <td>$row->id</td>
                  <td>$row->name</td>
                  <td>$row->city</td>
                  <td>
                     <a href='view.php?id=$row->id'  class='btn btn-primary'> View </a>
                     <a href='edit.php?id=$row->id' class='btn btn-info'> Edit </a>
                     <a href='recover.php?id=$row->id' class='btn btn-success'> Recover </a>
                     <a href='delete.php?id=$row->id' onclick='return confirm_delete()' class='btn btn-danger'> Delete </a>


                  </td>
              </tr>


            ";


                $serial++;

            }//end of foreach loop

            ?>
    </form>
</table>
<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>



<script type="text/javascript">

    function confirm_delete()
    {
        return confirm('are you sure?');
    }

</script>
<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>
<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }


    $("#recoverMultipleButton").click(function(){
        if(checkEmptySelection()) {
            alert("Empty Selection! Please select some record(s) first")
        }else {
            $("#selectionForm").submit();
        }
    }) ;



</script>

</body>
</html>
