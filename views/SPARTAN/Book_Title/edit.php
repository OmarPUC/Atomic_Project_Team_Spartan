<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Message\Message;

$msg = Message::message();

$obj = new \App\BookTitle\BookTitle();

$obj->setData($_GET);
$singleData = $obj->view();



echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">
    <style>
        body {
           background-color: #c1e2b3;
        }
    </style>
</head>
<body class="container">
<!--this is start-->
<div class="container">
    <h1 style="color: green
">Edit Book Information</h1>
<form action="update.php" method="post">

    <strong> Please Enter Book Name:</strong>
    <input type="text" name="bookTitle" value="<?php echo $singleData->book_title?>">
    <br>

    <strong>Please Enter Author Name: </strong>
    <input type="text" name="authorName" value="<?php echo $singleData->author_name?>">
    <br>
    <input type="hidden" name="id" value="<?php echo $singleData->id?>">

    <input type="submit" value="Update">

</form>
</div>
<!--this is end-->

<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>