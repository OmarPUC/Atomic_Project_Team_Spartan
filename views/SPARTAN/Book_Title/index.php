<?php
require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

$obj = new \App\BookTitle\BookTitle();

$allData = $obj->index();

use App\Message\Message;
use App\Utility\Utility;
$msg = Message::message();



################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) ){

    $someData =  $obj->search($_REQUEST);
}
$availableKeywords = $obj->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################





######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;


if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;



$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->indexPaginator($page,$itemsPerPage);
$allData= $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################






################## search  block 2 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $obj->search($_REQUEST);

if(isset($_REQUEST['search']) ) {
    $serial = 1;


    $allData=$someData;

}
################## search  block 2 of 5 end ################
?>





<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>

    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">



    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/bootstrap/css/jquery-ui.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body class="container">




<!-- required for search, block 4 of 5 start -->

<div style="margin-left: 70%">
    <form id="searchForm" action="index.php"  method="get" style="margin-top: 5px; margin-bottom: 10px ">
        <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
        <input type="checkbox"  name="byTitle"   checked  >By Title
        <input type="checkbox"  name="byAuthor"  checked >By Author
        <input hidden type="submit" class="btn-primary" value="search">
    </form>
</div>

<!-- required for search, block 4 of 5 end -->



  <a href="create.php" class="btn btn-primary">Create</a>
  <a href="../home.php" class="btn btn-primary">Home</a>


   <?php echo "<div>  <div align='center' class=' alert-info ' id='message'>  $msg </div>   </div>";   ?>
   <h1> Active List - Book Title</h1>


  <form  id="selectionForm" action="trash_multiple.php" method="post">

   <div class="nav navbar">

        <input class="btn btn-warning btn-lg" type="button" id="trashMultipleButton" value="Trash Multiple">
        <input class="btn  btn-danger btn-lg" type="button" id="deleteMultipleButton" value="Delete Multiple">

        <a href="pdf.php" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as PDF</a>
        <a href="xl.php" class="btn btn-lg btn-info"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-circle-arrow-down"></span> Download as Excel</a>
        <a href="email.php?list=1" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon-envelope"></span> Email This List</a>
        <a href="trashed.php" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-list-alt"></span> <span class="glyphicon glyphicon glyphicon-cog"></span> Trash List</a>


   </div>


   <table class="table table-bordered table-striped">

       <tr>

           <th>Check All <input type='checkbox' id='select_all' name='select_all' value='$row->id'></th>
           <th>Serial</th>
           <th>ID</th>
           <th>Book Title</th>
           <th>Author Name</th>
           <th>Action Buttons</th>

       </tr>

   <?php
   // $serial = 1;
    foreach($allData as $row){

     echo "
       <tr>
            <td><input type='checkbox' class='checkbox' name='multiple[]' value='$row->id'> </td>

            <td>$serial</td>
            <td>$row->id</td>
            <td>$row->book_title</td>
            <td>$row->author_name</td>
            <td>

                 <a href='view.php?id=$row->id' class='btn btn-primary'> <span class='glyphicon glyphicon-eye-open'> </span> View </a>
                 <a href='edit.php?id=$row->id'class='btn btn-success'> <span class='glyphicon glyphicon-pencil'> </span> Edit </a>
                 <a href='trash.php?id=$row->id' class='btn btn-warning'> <span class='glyphicon glyphicon-trash'> </span> Trash </a>
                 <a href='delete.php?id=$row->id' onclick='return confirm_delete()' class='btn btn-danger'> <span class='glyphicon glyphicon-remove'> </span> Delete </a>
                 <a href='email.php?id=$row->id' class='btn btn-primary'> <span class='glyphicon glyphicon-envelope'> </span> Email This Record </a>


            </td>

       </tr>

     ";

      $serial++;
     }//end of foreach loop

       ?>



   </table>
  </form>





<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div align="left" class="container">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;


        if($page>$pages) Utility::redirect("index.php?Page=$pages");

        if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->



<script>


       jQuery(

           function($) {
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
               $('#message').fadeIn (550);
               $('#message').fadeOut (550);
           }
       )
   </script>

   <script>

       function confirm_delete(){

           return confirm("Are You Sure?");

       }

   </script>


   <script>

       $('#deleteMultipleButton').click(function(){

            if(checkEmptySelection()){
                alert("Empty Selection! Please select some record(s) first")
            }
           else{
                var r = confirm("Are you sure you want to delete the selected record(s)?");

                if(r){
                    var selectionForm =   $('#selectionForm');
                    selectionForm.attr("action","delete_multiple.php");
                    selectionForm.submit();
                }
            }
       });


   </script>



<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>

<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }


    $("#trashMultipleButton").click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record(s) first")
        }else{

            $("#selectionForm").submit();

        }




   }) ;



</script>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block 5 of 5 end -->


</body>
</html>

























