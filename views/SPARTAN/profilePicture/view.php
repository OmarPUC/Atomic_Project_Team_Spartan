
<?php
use App\Utility\Utility;
use App\Message\Message;

require_once ("../../../vendor/autoload.php");

if(!isset($_GET['id'])){
    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
    Utility::redirect("index.php");
}
$obj = new\App\ProfilePicture\ProfilePicture();
$obj->setData($_GET);
$singleData = $obj->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>


</head>
<body class="container">

<h1>Profile Picture Information  </h1>
<a href="index.php" class="btn btn-primary">Back</a>

<table class="table table-bordered table-striped">

    <?php




    echo "

            <tr><td>ID:</td> <td>$singleData->id</td></tr>
            <tr><td>Name:</td> <td>$singleData->name</td></tr>
            <tr> <td>Profile Picture:</td> <td><img src='images/$singleData->profile_picture' height='100px' width='100px'></td> </tr>




            ";

    ?>

</table>

</body>
</html>
