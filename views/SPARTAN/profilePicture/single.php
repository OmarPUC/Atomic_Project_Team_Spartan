<?php
ob_start();

use App\Utility\Utility;
use App\Message\Message;

require_once ("../../../vendor/autoload.php");

if(!isset($_GET['id'])){
    Message::message("You can't visit view.php without id(i.e.; view.php?id=15");
}
$obj = new\App\ProfilePicture\ProfilePicture();
$obj->setData($_GET);
$singleData = $obj->view();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>

<h1>Profile Picture Information  </h1>

<table class="table table-bordered table-striped">

    <?php




    echo "

            <tr><td>ID:</td> <td>$singleData->id</td></tr>
            <tr><td>Name:</td> <td>$singleData->name</td></tr>
            <tr> <td>Profile Picture:</td> <td><img src='images/$singleData->profile_picture' height='100px' width='100px'></td> </tr>




            ";

    ?>

</table>

</body>
</html>
<?php
file_put_contents('contents.html', ob_get_clean());
\App\Utility\Utility::redirect("email.php?id=0".$singleData->id);
?>
