<?php
ob_start();

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
$obj = new\App\ProfilePicture\ProfilePicture();
$allData = $obj->index();

$msg = Message::message();

?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Edit</title>

    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/jquery-ui.css">

    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/bootstrap/js/jquery-ui.js"></script>


</head>

<body>





<h1> Active List of - Profile Picture </h1>

<table class="table table-bordered table-striped">



            <tr>
                <th>Serial</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action Buttons</th>
            </tr>

            <?php

            $serial = 1;

            foreach($allData as $row){
                echo "
        
        
        <td>$serial</td>
        <td>$row->id</td>
        <td>$row->name</td>
        <td> <img src='images/$row->profile_picture' height='100px' width='100px'>  </td>
        <td>  
        
        </td>
        </tr>
        
        ";
                $serial++;


            }
            ?>

</table>





</body>
</html>
<?php
file_put_contents('contents.html', ob_get_clean());
\App\Utility\Utility::redirect("email.php?list=1");
?>










