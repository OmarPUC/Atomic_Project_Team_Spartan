<?php
require_once("../../../vendor/autoload.php");

use App\profilePicture\profilePicture;
use App\Utility\Utility;

$obj = new profilePicture();

$obj->setData($_GET);

$obj->trash();

Utility::redirect("trashed.php");