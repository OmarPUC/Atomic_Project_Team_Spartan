<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/summery.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Summery Of Organization</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>

</head>
<body>

<div class="container">
    <div id="content">
        <h1>Add Summery</h1>
        <form action="store.php" method="post">
            <div class= "wrapper">
                <input type="text" name="name" placeholder="User Name" required>
            </div>
            <div class="text">
                <textarea cols="40" rows="3" name="summery" placeholder="Write Here...." required></textarea>
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>

            <div class="msg">
                  <?php
                  require_once ("../../../vendor/autoload.php");

                  $msg = \App\Message\Message::message();

                  echo "<div id='message'>".$msg."</div>";

                  ?>
             </div>
        </form>
    </div>
</div>

</body>
</html>