<?php
require_once("../../../vendor/autoload.php");

use  App\SummeryOfOrg\Summery;
use App\Utility\Utility;

$obj = new Summery();

$obj->setData($_POST);

$obj->update();

Utility::redirect("index.php");