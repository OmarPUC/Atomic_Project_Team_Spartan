<?php
use App\Utility\Utility;
use App\Message\Message;
require_once ("../../../vendor/autoload.php");

if(!isset($_GET['id'])) {

    Message::message("You can't visit view.php without id (ex: view.php?id=23)");
    Utility::redirect("index.php");
}


$obj = new \App\Birthday\Birthday();

$obj->setData($_GET);

$singleData  =  $obj->view();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

</head>
<body class="container">


<h1> Single Birthday Info </h1>
<a href="index.php" class="btn btn-primary">Back</a>


<table class="table table-bordered table-striped">



    <?php




    echo "

            <tr><td>ID:</td> <td>$singleData->id</td></tr>
            <tr><td>Name:</td> <td>$singleData->name</td></tr>
            <tr> <td>Birthday:</td> <td>$singleData->birth_day</td></tr>




            ";






    ?>

</table>











</body>
</html>
