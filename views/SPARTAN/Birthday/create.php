
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BirthDay</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../../resources/style/book_title.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">

</head>
<body>
<!-- carousel-section -->


<div class="container">
    <div id="content">
        <h1>BirthDay</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder=" Name" required>
            </div>
            <div class="wrapper">
                <input type="text" id="datepicker" name="date" placeholder="birthday" required>
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>

            <?php

            require_once("../../../vendor/autoload.php");

            use App\Message\Message;

            $msg = Message::message();

            echo "<div>  
                 <div id='message' style='color: red;padding: 10px;font-size: 18px'>  $msg </div>
            </div>";

            ?>
        </form>
    </div>
</div>

<script src="../../../resources/bootstrap/js/jquery.js"></script>
<script src="../../../resources/bootstrap/js/jquery1.12.4.js"></script>
<script src="../../../resources/bootstrap/js/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
</script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

<script src="../../../resources/bootstrap/js/parallax.min.js"></script>
<script src="../../../resources/bootstrap/js/animatescroll.min.js"></script>
<script src="../../../resources/bootstrap/js/css3-animate-it.js"></script>
</body>
</html>