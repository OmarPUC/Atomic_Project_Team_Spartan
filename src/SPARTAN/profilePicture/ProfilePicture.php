<?php
namespace App\profilePicture;



use App\Message\Message;
use App\Model\Database;
use PDO;

class ProfilePicture extends Database
{
    public $id;
    public $user_name;
    public $profilePic;

    public function setData($rcv){
        if (array_key_exists('id',$rcv)){

            $this->id =  $rcv['id'];
        }

        if (array_key_exists('name',$rcv)){

            $this->user_name =  $rcv['name'];
        }
        if (array_key_exists('profilePic',$rcv)){

            $this->profilePic =  $rcv['profilePic'];
        }
    }

    public function store(){

        $name= $this->user_name;
        $profileImg = $this->profilePic;

        $dataArray = array($name,$profileImg);

        $sql = "INSERT INTO `profile_picture` (`name`, `profile_picture`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);
        if ($result){

            Message::message("Successfully! Picture Inserted");
        }else {
            Message::message("Error! Picture does not inserted...");
        }
    }
    public function index(){

        $sqlQuery = "Select * from profile_picture where is_trashed='No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view(){

        $sqlQuery = "Select * from profile_picture where id=".$this->id;

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update(){

        $sqlQuery = "UPDATE profile_picture SET  name = ?,profile_picture = ? WHERE id = $this->id;";

        //    Utility::dd($sqlQuery);


        $dataArray = array($this->user_name, $this->profilePic);

        $STH = $this->db->prepare($sqlQuery);

        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success! Data has been updated successfully.");
        }
        else {
            Message::message("Failure! Update is not possible due to an error.");

        }

    }// end of store()
    public function trash(){

        $sqlQuery = "UPDATE profile_picture SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been trashed successfully.");
        }
        else {
            Message::message("Failure! Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from profile_picture where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE profile_picture SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been recovered successfully.");
        }
        else {
            Message::message("Failure! Trashed is not possible due to an error.");

        }

    }





    public function delete(){

        $sqlQuery = "DELETE from profile_picture  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been deleted successfully.");
        }
        else {
            Message::message("Failure! Delete operation is not possible due to an error.");

        }

    }

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byImage']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `profile_picture` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byImage']) ) $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byImage']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_trashed` ='No' AND `profile_picture` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->profile_picture);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->profile_picture);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from profile_picture  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



}