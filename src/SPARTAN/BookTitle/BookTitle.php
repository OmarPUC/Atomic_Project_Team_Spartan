<?php
namespace App\BookTitle;

use App\Utility\Utility;
use PDO;
use App\Model\Database;
use App\Message\Message;

class BookTitle extends Database
{
    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($postArray){
        if(array_key_exists("id",$postArray))
        {
            $this->id = $postArray['id'];
        }

        if (array_key_exists('bookTitle',$postArray)){

            $this->bookTitle = $postArray['bookTitle'];
        }
        if (array_key_exists('authorName',$postArray)){

            $this->authorName = $postArray['authorName'];
        }
    }//set Data

    public function store(){

        $bookName = $this->bookTitle;
        $authorName = $this->authorName;

        $dataArray = array($bookName,$authorName);

        $sqlQuery ="INSERT INTO `book_title` (`book_title`, `author_name`) VALUES (?,?);";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }


    }

    /**
     * @return array
     */
    public function index(){

        $sqlQuery = "Select * from book_title where is_trashed = 'No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view(){

        $sqlQuery = "Select * from book_title where id=".$this->id;

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }


    public function update(){

        $sqlQuery = "UPDATE book_title SET book_title = ?,author_name = ? WHERE id = $this->id;";

        //    Utility::dd($sqlQuery);


        $dataArray = array($this->bookTitle, $this->authorName);

        $STH = $this->db->prepare($sqlQuery);

        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success :) Data has been updated successfully.");
        }
        else {
            Message::message("Failure :( Update is not possible due to an error.");

        }

    }// end of update()

    
    public function trash(){

        $sqlQuery = "UPDATE book_title SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from book_title where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE book_title SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }





    public function delete(){

        $sqlQuery = "DELETE from book_title  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Delete operation is not possible due to an error.");

        }

    }
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='No' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from book_title  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }












}//End BookTitle Class
