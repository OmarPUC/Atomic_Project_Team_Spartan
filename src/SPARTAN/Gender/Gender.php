<?php
namespace App\Gender;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class Gender extends Database
{
    public $id;
    public $name;
    public $gender;

    public function setData($rcv){
        if (array_key_exists('id',$rcv)){
            $this->id = $rcv['id'];
        }
        if (array_key_exists('name',$rcv)){
            $this->name = $rcv['name'];
        }
        if (array_key_exists('Gender',$rcv)){
            $this->gender = $rcv['Gender'];
        }
    }//end of setData

    public function store(){

        $name = $this->name;
        $gender = $this->gender;
        $dataArray = array($name,$gender);

        $sql = "INSERT INTO `gender` (`user_name`, `gender`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully! Inserted Data...");
        }else {

            Message::message("Error!! Check Please..");
        }
    }
    public function index(){

        $sqlQuery = "Select * from gender where is_trashed='No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view(){

        $sqlQuery = "Select * from gender where id=".$this->id;

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update(){

        $sqlQuery = "UPDATE gender SET user_name = ?,gender = ? WHERE id = $this->id;";

        //    Utility::dd($sqlQuery);


        $dataArray = array($this->name, $this->gender);

        $STH = $this->db->prepare($sqlQuery);

        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success! Data has been updated successfully.");
        }
        else {
            Message::message("Failure! Update is not possible due to an error.");

        }

    }// end of store()
    public function trash(){

        $sqlQuery = "UPDATE gender SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been trashed successfully.");
        }
        else {
            Message::message("Failure! Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from gender where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE gender SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been trashed successfully.");
        }
        else {
            Message::message("Failure! Trashed is not possible due to an error.");

        }

    }





    public function delete(){

        $sqlQuery = "DELETE from gender  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success! Data has been deleted successfully.");
        }
        else {
            Message::message("Failure! Delete operation is not possible due to an error.");

        }

    }
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND (`user_name` LIKE '%".$requestArray['search']."%' OR `gender` LIKE '%".$requestArray['search']."')";
        if(isset($requestArray['byName']) && !isset($requestArray['byGender']) ) $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `user_name` LIKE '".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byGender']) )  $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `gender` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->user_name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->user_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->gender);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->gender);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from gender  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



}//end of gender class