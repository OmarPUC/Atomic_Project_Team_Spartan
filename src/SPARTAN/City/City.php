<?php
namespace App\City;
use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class City extends Database
{
    public $id;
    public $name;
    public $city;

    public function setData($rcv){
        if (array_key_exists('id',$rcv)){
            $this->id = $rcv['id'];
        }

        if (array_key_exists('name',$rcv)){

            $this->name = $rcv['name'];
        }
        if (array_key_exists('country',$rcv)){

            $this->city = $rcv['country'] ;
        }
    }

    public function store(){

        $name = $this->name;
        $country = $this->city;
        $dataArray = array($name,$country);

        $sql = "INSERT INTO `city` (`name`, `city`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Success! Inserted Data.....");
        }else{

            Message::message("Error!! Data Does Not Inserted....");
        }
    }
    public function index(){

        $sqlQuery = "Select * from city where is_trashed='No'";

        $STH = $this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();
        return $allData;
    }

    public function view(){

        $sqlQuery = "Select * from city where id=".$this->id;

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData =$STH->fetch();
        return $singleData;
    }
    public function update(){

        $sqlQuery = "UPDATE city SET name = ?,city = ? WHERE id = $this->id;";

        //    Utility::dd($sqlQuery);


        $dataArray = array($this->name, $this->city);

        $STH = $this->db->prepare($sqlQuery);

        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Success :) Data has been updated successfully.");
        }
        else {
            Message::message("Failure :( Update is not possible due to an error.");

        }

    }// end of store()
    public function trash(){

        $sqlQuery = "UPDATE city SET is_trashed=NOW() WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }
    public function trashed(){

        $sqlQuery = "Select * from city where is_trashed <> 'No'";

        $STH =$this->db->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData =$STH->fetchAll();
        return $allData;
    }
    public function recover(){

        $sqlQuery = "UPDATE city SET is_trashed='No' WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been trashed successfully.");
        }
        else {
            Message::message("Failure :( Trashed is not possible due to an error.");

        }

    }





    public function delete(){

        $sqlQuery = "DELETE from city  WHERE id = $this->id;";

        $result=  $this->db->exec($sqlQuery);


        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Delete operation is not possible due to an error.");

        }

    }
    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byCity']) )  $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `city` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byCity']) ) $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['byCity']) && !isset($requestArray['byName']) )  $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `city` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->city);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from city  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



}